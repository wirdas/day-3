package app.obj;

public class Mahasiswa {
	public String name;
	public String telp;
	
	public Mahasiswa(String name, String telp) {
		super();
		this.name = name;
		this.telp = telp;
	}
	
	public void lemparData() 
	{
		System.out.println("Nama :" + name);
		System.out.println("Telp :" + telp);
	}
}